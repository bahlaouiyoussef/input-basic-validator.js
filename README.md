# input-basic-validator.js

[![npm version](https://badge.fury.io/js/input-basic-validator.svg)](//npmjs.com/package/input-basic-validator)

A string basic validator

## Strings only

**This module validates strings only.**

## Installation and Usage

## Server-side usage

### `npm install input-basic-validator`

```javascript
const iv = require('input-basic-validator');

iv.isEmpty(''); // => true
```

Import only a subset of the module:
```javascript
const isEmpty = require('input-basic-validator/lib/isEmpty');
isEmpty('hello world'); // => false
// Or
const isEmpty = require('input-basic-validator').isEmpty;
isEmpty('hello world'); // => false
```

## Validators

### ****isEmpty(str, options)****

Check if the given string is empty(has length of zero).<br/>
options default value: `{ ignore_space: false }`<br/>
if `ignore_space` is set to true, spaces will be ignored <br/>
Example:<br/>
```javascript
isEmpty('    ', { ignore_space: true }); // => true
isEmpty('    ', { ignore_space: false }); // => false
```

### ****isNumeric(str, options)****

Check if the given string contains only digits and valid symbols.<br/>
options default value: `{ no_symbols: false }`<br/>
If `no_symbols` set to true, the symbols ['+', '-', '.'] will be rejected.<br/>
Example:<br/>
```javascript
isNumeric('+123.456', { no_symbols: false }); // => true
isNumeric('+123.456', { no_symbols: true }); // => false
```

### ****isInt(str, options)****

Check if the given string is integer.
options can contain the following flags:<br/>
- `lt`:&nbsp;&nbsp;to check if the value less than.
```javascript
isInt('10', { lt: 11 }); // => true
isInt('11', { lt: 11 }); // => false
```
- `gt`:&nbsp;&nbsp;to check if the value greater than.
```javascript
isInt('12', { gt: 11 }); // => true
isInt('11', { gt: 11 }); // => false
```
- `lte`:&nbsp;&nbsp;to check if the value less than or equal.
```javascript
isInt('11', { lte: 11 }); // => true
isInt('12', { lte: 11 }); // => false
```
- `gte`:&nbsp;&nbsp;to check if the value greater than or equal.
```javascript
isInt('11', { gte: 11 }); // => true
isInt('10', { gte: 11 }); // => false
```
### ****isEmail(str, options)****

Check if the given string is email.<br/>
options default value: `{ ignore_max_length: false }`<br/>
`max_length` equal to 254.<br/>
If `ignore_max_length` set to true the email can contain more than `max_length` characters.<br/>
Example:<br/>
```javascript
isEmail('foo@bar.com'); // => true
isEmail('foobar.com'); // => false
```

### ****contains(a, b, options)****

Check if `a` contains `b`.<br/>
options default value: `{ case_sensitive: true }`<br/>
If `case_sensitive` set to false then the evaluation will ignore the case of the given strings.<br/>
Example:<br/>
```javascript
contains('hello world', 'llo'); // => true
contains('HELLO WORLD', 'llo', { case_sensitive: false }); // => true
contains('HELLO WORLD', 'llo', { case_sensitive: true }); // => false
```

### ****equals(a, b, options)****

Check if `a` match `b`.<br/>
options default value: `{ case_sensitive: true }`<br/>
If `case_sensitive` set to false then the evaluation will ignore the case of the given strings.<br/>
Example:<br/>
```javascript
equals('abc', 'abc'); // => true
equals('abc', 'ABC', { case_sensitive: false }); // => true
equals('abc', 'ABC', { case_sensitive: true }); // => false
```

### ****matches(str, pattern, modfiers)****
Check if str matche pattern.<br/>
Example:<br/>
```javascript
matches('foo', '^[a-z]+$', 'i'); // => true
matches('FOO', /^[a-z]+$/i); // => true
```
