const stringTypeError = require('./tools/stringTypeError');
const defaultMaxLength = 254;
const regex = /^[a-z0-9.%_+-]+@[a-z0-9-]+\.[a-z0-9\.]{2,}$/i

function isEmail(str, options = null) {
	stringTypeError(str);

	options = options || {};
	let ignore_max_length = options.ignore_max_length || false;
	if (ignore_max_length === false && str.length > defaultMaxLength)
		return (false);
	return (regex.test(str));
}

module.exports = isEmail;
