const stringTypeError = require('./tools/stringTypeError');

function contains(a, b, options = null) {
	stringTypeError(a);
	stringTypeError(b);

	options = options || {};
	let case_sensitive = options.case_sensitive === false ? options.case_sensitive : true;

	if (case_sensitive === false) {
		a = a.toLowerCase();
		b = b.toLowerCase();
	}
	return (a.indexOf(b) >= 0);
}

module.exports = contains;
