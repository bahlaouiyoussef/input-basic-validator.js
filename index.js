const _isEmpty = require('./lib/isEmpty');
const _isNumeric = require('./lib/isNumeric');
const _isInt = require('./lib/isInt');
const _contains = require('./lib/contains');
const _equals = require('./lib/equals');
const _isEmail = require('./lib/isEmail');
const _matches = require('./lib/matches');

module.exports = {
	'isEmpty': _isEmpty,
	'isNumeric': _isNumeric,
	'isInt': _isInt,
	'contains': _contains,
	'equals': _equals,
	'isEmail': _isEmail,
	'matches': _matches
};
